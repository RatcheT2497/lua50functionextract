This is a tool I wrote to find functions and their argument counts in a Lua 5.0 bytecode listing file.

Input: Bytecode listing generated using Lua 5.0's luac.

Output: CSV file containing the function names in one column and their argument count (or counts, if there's multiple) in the rest of the columns.

It could work for newer lua versions, but I haven't tested that yet.