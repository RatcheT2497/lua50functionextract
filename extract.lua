------------------------------------------------------------------------------------------------
------------------------------------    HELPER FUNCTIONS    ------------------------------------
------------------------------------------------------------------------------------------------
--copied from one of the lua docs


function table_copy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[table_copy(orig_key)] = table_copy(orig_value)
        end
        setmetatable(copy, table_copy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function file_exists(path)
    local f = io.open(path, "rb")
    if f then f:close() end
    return f ~= nil
end

function to_string(t)
    if type(t) == "string" then return "\"" .. t .. "\""
    elseif type(t) == "table" then
        local str = "{"
        for i = 1, #t do 
            str = str .. to_string(t[i])
            if i ~= #t then str = str .. "," end
        end
        str = str .. "}"
        return str
    else return tostring(t) end
end

---------------------------------------------------------------------------------------------------------
------------------------------------    LISTING PARSING FUNCTIONS    ------------------------------------
---------------------------------------------------------------------------------------------------------

function line_tokenize(s)
    local temp, tokens = "", {}
    local function flush()
        if temp ~= "" then
            tokens[#tokens + 1] = temp
            temp = ""
        end
    end
    for i = 1, #s do
        local c = s:sub(i, i)
        if c == ' ' or c == '\t' then
            flush()
        else
            temp = temp .. c
        end
    end
    if temp ~= "" then
        flush()
    end
    return tokens
end

function line_parse(s)
    local tokens = line_tokenize(s)
    local result = {
        asm_line = 0,
        src_line = 0,
        name = "",
        args = {},
        comment = nil,
    }
    setmetatable(result, {
        __tostring = function(t)
            local s = "";
            s = s .. "Asm Line No.: " .. t.asm_line .. "\n"
            s = s .. "Src Line No.: " .. t.src_line .. "\n"
            s = s .. "Name: " .. t.name .. "\n"
            s = s .. "Args: " .. to_string(t.args) .. "\n"
            s = s .. "Comment: " .. tostring(t.comment)
            return s
        end
    })
    if tonumber(tokens[1]) and tokens[2]:sub(1, 1) == "[" and tokens[2]:sub(-1, -1) == "]" then
        result.asm_line = tonumber(tokens[1])
        result.src_line = tonumber(tokens[2]:sub(2, -2))
        result.name = tokens[3]
        
        result.args[1] = tonumber(tokens[4])
        result.args[2] = tonumber(tokens[5])
        local semicolon_pos = 7
        if tokens[6] == ";" then 
            semicolon_pos = 6
        else
            result.args[3] = tonumber(tokens[6])
        end
        if tokens[semicolon_pos] and tokens[semicolon_pos + 1] then
            result.comment = tokens[semicolon_pos + 1]
        end
        return result
    end
    return nil
end

-----------------------------------------------------------------------------------------------------------
------------------------------------    INSTRUCTION BLOCK FUNCTIONS    ------------------------------------
-----------------------------------------------------------------------------------------------------------

local funcs = {}
local found = {}
function handle_block(block)
    local first, last = block[1], block[#block]
    if first.name == "GETGLOBAL" and first.comment and last.name == "CALL" then
        local blockargcount = last.args[2] - 1
        local funcname = first.comment
        local savedargcount = funcs[funcname]
        if not found[funcname] then found[funcname] = {} end

        if not savedargcount then
            funcs[funcname] = blockargcount
            found[funcname][blockargcount] = true
        else
            if type(savedargcount) == "number" then
                if savedargcount ~= blockargcount then
                    funcs[funcname] = {savedargcount, blockargcount}
                    found[funcname][blockargcount] = true
                end
            elseif type(savedargcount) == "table" then
                if not found[funcname][blockargcount] then
                    funcs[funcname][#funcs[funcname + 1]] = blockargcount
                    found[funcname][blockargcount] = true
                end
            end
        end
    end
end

------------------------------------------------------------------------------------------------
------------------------------------    START OF PROGRAM    ------------------------------------
------------------------------------------------------------------------------------------------
local input_path, output_path
if not arg[1] then 
    print("Usage: extract [path to bytecode listing] [output path of csv]");
    return; 
end

input_path = arg[1]
output_path = arg[2]
if not arg[2] then 
    print("no output path, defaulting to " .. input_path .. ".csv"); 
    output_path = input_path .. ".csv"; 
end

if file_exists(input_path) then
    local temp, lastnumber = {}, -1
    for line in io.lines(input_path) do
        local parsed = line_parse(line)
        if parsed then
            if lastnumber ~= parsed.src_line then
                if lastnumber ~= -1 then
                    handle_block(temp)
                end
                lastnumber = parsed.src_line
                temp = {}
            end
            temp[#temp + 1] = table_copy(parsed)
        end
    end
    if #temp > 0 then
        handle_block(temp)
    end

    local s = ""
    for k, v in pairs(funcs) do
        s = s .. k .. ","
        if type(v) == "number" then
            s = s .. v .. ",\n"
        elseif type(v) == "table" then
            table.sort(v)
            for i = 1, #v do
                s = s .. v[i] .. ","
            end
            s = s .. "\n"
        end
    end
    local output_file = io.open(output_path, "w")
    output_file:write(s)
    output_file:close()
end